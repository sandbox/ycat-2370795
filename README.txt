CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

 INTRODUCTION
 ------------
 The Views exposed containers module allows to wrap multiple views exposed
 filters in containers (divs). This module allows you to create as many
 container as you want from Views UI.


 REQUIREMENTS
 ------------
 This module requires the following modules:
 * Views (https://drupal.org/project/views)


 INSTALLATION
 ------------
  * Install as you would normally install a contributed drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.


CONFIGURATION
-------------
  * The module has no global configuration. Exposed filters can be configured
    from the views advanced fieldset under exposed form style. A new
    'Containers' option should be available.

  * This module doesn't integrate with better exposed filters, however form
    type can easily be altered using hook_form_alter(). See: http://goo.gl/nXcL1

    Sample code for turning a select to radios, replace 'my_module' with your
    module name (or theme name if in template.php), 'view_name' with the name of
    the view you are altering and 'field_name' with the exposed field name:

    function my_module_form_alter(&$form, &$form_state, $form_id) {
      // Check if we are on the right form.
      if (($form_id === 'views_exposed_form') && ($form_state['view']->name === 'view_name')) {
        // Loop through our exposed filters.
        foreach ($form['filters'] as $key => $value) {
          // Replace field_name with the field you want to alter.
          if (isset($form['filters'][$key]['field_name'])) {
            $form['filters'][$key]['field_name']['#type'] = 'radios';
            break;
          }
        }
      }
    }


MAINTAINERS
-----------
Current maintainers:
 * Yohann Catherine (ycat974) - https://www.drupal.org/user/1948310

This project has been sponsored by:
 * DOGHOUSE
   We're Doghouse and we're a full service Digital Agency with offices in Perth
   and Melbourne. Visit http://dhmedia.com.au for more information.
