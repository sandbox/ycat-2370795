<?php

/**
 * @file
 * Describes the views_exposed_containers views plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function views_exposed_containers_views_plugins() {
  return array(
    'exposed_form' => array(
      'views_exposed_containers' => array(
        'title' => t('Containers'),
        'help' => t('Wrap exposed filters in divs.'),
        'help topic' => '',
        'handler' => 'ViewsExposedContainersPlugin',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'parent' => 'basic',
      ),
    ),
  );
}
