<?php
/**
 * @file
 * Provides a grouped form exposed form plugin for View 3.x.
 */

class ViewsExposedContainersPlugin extends views_plugin_exposed_form_basic {

  /**
   * Views UI summary title.
   */
  function summary_title() {
    return t('Settings');
  }

  /**
   * Option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['views_exposed_containers'] = array('default' => array());
    return $options;
  }

  /**
   * The exposed form style settings.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $containers = explode("\n", $this->options['views_exposed_containers']['containers']);
    $containers['no-container'] = t('- No container -');

    // Additional classes for the form.
    $form['views_exposed_containers']['classes'] = array(
      '#type' => 'textfield',
      '#weight' => 1,
      '#title' => t('CSS Class'),
      '#description' => t('The CSS class names will be added to the exposed form. You may define multiples classes separated by spaces.'),
      '#default_value' => $this->options['views_exposed_containers']['classes'],
    );

    // Textarea for containers.
    $form['views_exposed_containers']['containers'] = array(
      '#type' => 'textarea',
      '#weight' => 5,
      '#title' => t('Containers'),
      '#description' => t('Enter a list of containers for this form (one container per line)'),
      '#default_value' => $this->options['views_exposed_containers']['containers'],
    );

    // Drag and and drop interface.
    $weight_delta = count($this->display->handler->get_handlers('filter'));

    foreach ($this->display->handler->get_handlers('filter') as $filter) {
      // Skip filters that aren't exposed on our view.
      if (!$filter->options['exposed']) {
        continue;
      }

      // Retrieve the exposed field label.
      $label = $filter->options['expose']['identifier'];
      $field_label = ($filter->options['expose']['label'])
       ? $filter->options['expose']['label']
       : $label;

      // Build the container options for each exposed fields.
      $form['views_exposed_containers']['container-' . $label]['container'] = array(
        '#type' => 'select',
        '#title' => 'Container for ' . $field_label,
        '#filter_field' => $filter,
        '#options' => $containers,
        '#title_display' => 'invisible',
        '#default_value' => $this->options['views_exposed_containers']['container-' . $label]['container'],
      );

      // Row weights settings.
      $form['views_exposed_containers']['container-' . $label]['weight'] = array(
        '#type' => 'weight',
        '#title' => $field_label,
        '#filter_field' => $filter,
        '#delta' => $weight_delta,
        '#title_display' => 'invisible',
        '#default_value' => $this->options['views_exposed_containers']['container-' . $label]['weight'],
      );
    }

    $form['views_exposed_containers']['#theme'][] = 'views_exposed_containers_reorder_filter_form';
  }

  /**
   * Tweak the exposed filter form to show containers form options.
   */
  function exposed_form_alter(&$form, &$form_state) {
    // Default Class.
    $form['#attributes']['class'][] = 'views-exposed-form';
    // CSS Class settings.
    if (!empty($this->options['views_exposed_containers']['classes'])) {
      $form['#attributes']['class'][] = $this->options['views_exposed_containers']['classes'];
    }

    $form['#theme'] = array('');
    $form['filters'] = array(
      '#weight' => -10,
    );

    // Store our containers in an array.
    $containers = explode("\n", $this->options['views_exposed_containers']['containers']);
    $form['filters']['no-container'] = array();

    // Theme our containters/wrappers using FAPI container type.
    foreach ($containers as $key => $container) {
      $form['filters'][$key] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array(
            'views-exposed-form__container',
            'views-exposed-form__container--' . $container,
          ),
        ),
      );
    }

    $fields = $this->options['views_exposed_containers'];
    unset($fields['containers']);

    // Add filters into wrappers.
    foreach ($fields as $field => $container) {
      // Remove group string in front of our fields.
      $field = str_replace('container-', '', $field);

      // Build the no-container group first, then build the containers.
      if (isset($form[$field]) && is_array($form[$field])) {
        // Retrieve the field label.
        $field_label = (isset($form['#info']['filter-' . $field]['label']))
          ? $form['#info']['filter-' . $field]['label']
          : NULL;

        // Group existing options + form weights together.
        $grouping = $form['filters'][$container['container']][$field] = $form[$field] +
          array(
            '#weight' => $container['weight'],
            '#title' => $field_label,
            '#attributes' => array(
              'class' => array(
                'view-exposed-form__element',
                'view-exposed-form__element--' . drupal_clean_css_identifier($field),
              ),
            ),
          );

        if ($container['container'] == 'no-container') {
          $form['filters']['no-container'][$field] = $grouping;
        }
        else {
          $form['filters'][$container['container']][$field] = $grouping;
        }
      }
      unset($form[$field]);
    }

    // Bring back buttons, filters and default views features into the form.
    // Views reset button.
    if (!empty($this->options['reset_button'])) {
      $form['reset'] = array(
        '#value' => $this->options['reset_button_label'],
        '#type' => 'submit',
      );
    }

    // Views submit button.
    $form['submit']['#value'] = $this->options['submit_button'];
    $form['submit']['#attributes']['class'][] = 'views-exposed-form__submit';

    // Check if there is exposed sorts for this view.
    $exposed_sorts = array();
    foreach ($this->view->sort as $id => $handler) {
      if ($handler->can_expose() && $handler->is_exposed()) {
        $exposed_sorts[$id] = check_plain($handler->options['expose']['label']);
      }
    }

    // Exposed sorts.
    if (count($exposed_sorts)) {
      $form['sort_by'] = array(
        '#type' => 'select',
        '#options' => $exposed_sorts,
        '#title' => $this->options['exposed_sorts_label'],
        '#attributes' => array(
          'class' => array(
            'view-exposed-form__sort',
            'view-exposed-form__sort--by',
          ),
        ),
      );
      $sort_order = array(
        'ASC' => $this->options['sort_asc_label'],
        'DESC' => $this->options['sort_desc_label'],
      );
      if (isset($form_state['values']['sort_by']) && isset($this->view->sort[$form_state['values']['sort_by']])) {
        $default_sort_order = $this->view->sort[$form_state['values']['sort_by']]->options['order'];
      }
      else {
        $first_sort = reset($this->view->sort);
        $default_sort_order = $first_sort->options['order'];
      }

      if (!isset($form_state['values']['sort_by'])) {
        $keys = array_keys($exposed_sorts);
        $form_state['values']['sort_by'] = array_shift($keys);
      }

      if ($this->options['expose_sort_order']) {
        $form['sort_order'] = array(
          '#type' => 'select',
          '#options' => $sort_order,
          '#title' => t('Order'),
          '#default_value' => $default_sort_order,
          '#attributes' => array(
            'class' => array(
              'view-exposed-form__sort',
              'view-exposed-form__sort--order',
            ),
          ),
        );
      }
      $form['submit']['#weight'] = 10;
      if (isset($form['reset'])) {
        $form['reset']['#weight'] = 10;
        $form['reset']['#attributes']['class'][] = 'views-exposed-form__reset';
      }
    }

    // Views pager.
    $pager = $this->view->display_handler->get_plugin('pager');
    if ($pager) {
      $pager->exposed_form_alter($form, $form_state);
      $form_state['pager_plugin'] = $pager;
    }

    // Apply autosubmit values.
    if (!empty($this->options['autosubmit'])) {
      $form = array_merge_recursive($form, array('#attributes' => array('class' => array('ctools-auto-submit-full-form'))));
      $form['submit']['#attributes']['class'][] = 'ctools-use-ajax';
      $form['submit']['#attributes']['class'][] = 'ctools-auto-submit-click';
      $form['#attached']['js'][] = drupal_get_path('module', 'ctools') . '/js/auto-submit.js';

      if (!empty($this->options['autosubmit_hide'])) {
        $form['submit']['#attributes']['class'][] = 'js-hide';
      }
    }
  }
}
