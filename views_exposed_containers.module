<?php

/**
 * @file
 * Register View API information and the container form in views UI.
 */

/**
 * Implements hook_views_api().
 */
function views_exposed_containers_views_api() {
  return array(
    'api' => 3.0,
  );
}


/**
 * Implements hook_theme().
 */
function views_exposed_containers_theme() {
  return array(
    'views_exposed_containers_reorder_filter_form' => array(
      'render element' => 'form',
    ),
  );
}


/**
 * Theme callback to render the option form as a draggable table.
 */
function theme_views_exposed_containers_reorder_filter_form($vars) {
  $form = $vars['form'];
  $rows = array();

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['weight'])) {
      $container = (is_numeric($form[$key]['container']['#default_value']))
       ? $form[$key]['container']['#default_value']
       : 0;

      $row = array();
      $row[] = $form[$key]['weight']['#title'];

      $form[$key]['weight']['#attributes']['class'] = array('weight', 'weight-' . $container);
      $form[$key]['container']['#attributes']['class'] = array('container-select', 'field-container-' . $container);

      $row[] = drupal_render($form[$key]['container']);
      $row[] = drupal_render($form[$key]['weight']);
      $class = array('draggable');
      $styles = array();
      $rows[$container][] = array(
        'data' => $row,
        'class' => $class,
        'id' => 'display-row-' . $key,
        'style' => $styles,
      );
    }
  }

  $containers = explode("\n", $form['containers']['#default_value']);
  $containers['no-container'] = t('- No container -');
  $table_rows = array();

  // Build our table rows.
  foreach ($containers as $key => $container) {
    $table_rows[] = array(
      array(
        'data' => '<strong>' . ucfirst($container) . '</strong>',
        'colspan' => 3,
      ),
    );

    // Prevents warnings if the container is empty.
    if (!empty($rows[$key])) {
      foreach ($rows[$key] as $row) {
        $table_rows[] = $row;
      }
    }
  }

  // Bring in the javascript to make the table drag-and-droppable.
  foreach ($containers as $key => $container) {
    drupal_add_tabledrag('reorder-container-filters', 'match', 'sibling', 'container-select', 'field-container-' . $key, NULL, FALSE);
    drupal_add_tabledrag('reorder-container-filters', 'order', 'sibling', 'weight', 'weight-' . $key);
  }

  $output = drupal_render($form['override']);

  // Theme the table.
  $output .= theme('table', array(
    'header' => array('Exposed Filters', 'Container', 'Weight'),
    'rows' => $table_rows,
    'attributes' => array('id' => 'reorder-container-filters'),
    ));
  $output .= drupal_render_children($form);

  return $output;
}
